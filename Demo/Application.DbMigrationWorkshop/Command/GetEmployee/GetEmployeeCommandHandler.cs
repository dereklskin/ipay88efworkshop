﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dom = Domain.DbMigrationWorkshop;

namespace Application.DbMigrationWorkshop.Command
{
    public class GetEmployeeCommandHandler : IRequestHandler<GetEmployeeCommand, List<Employee>>
    {
        private readonly Dom.IDemoRepository _demoRepo;
        public GetEmployeeCommandHandler(Dom.IDemoRepository demoRepo)
        {
            this._demoRepo = demoRepo;
        }

        public async Task<List<Employee>> Handle(GetEmployeeCommand request, CancellationToken cancellationToken)
        {
            var result = new List<Employee>();
            var lsEmployee = await _demoRepo.GetAllEmployeeAsync();
            lsEmployee.ForEach(x => { result.Add(new Employee(x.EmployeeId, x.Name, x.MaritalStatus, x.JobTitle, x.PhoneNumber, x.Remarks, x.HiredDate)); });
            return result;
        }
    }
}
