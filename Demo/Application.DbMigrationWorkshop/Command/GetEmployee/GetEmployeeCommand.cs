﻿using MediatR;
using System.Collections.Generic;

namespace Application.DbMigrationWorkshop.Command
{
    public class GetEmployeeCommand : IRequest<List<Employee>>
    {
        public GetEmployeeCommand() { }
    }
}
