﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Dom = Domain.DbMigrationWorkshop;

namespace Application.DbMigrationWorkshop.Command
{
    public class AddEmployeeCommandHandler : IRequestHandler<AddEmployeeCommand, bool>
    {
        private readonly Dom.IDemoRepository _demoRepo;
        public AddEmployeeCommandHandler(Dom.IDemoRepository demoRepo)
        {
            this._demoRepo = demoRepo;
        }
        public async Task<bool> Handle(AddEmployeeCommand request, CancellationToken cancellationToken)
        {
            var rnd = new Random();
            var employee = new Dom.Employee(rnd.Next(), request.Employee.Name, request.Employee.MaritalStatus, request.Employee.JobTitle, request.Employee.PhoneNumber, request.Employee.Remarks, request.Employee.HiredDate);
            await Task.Run(() => _demoRepo.AddEmployeeAsync(employee));
            return true;
        }
    }
}
