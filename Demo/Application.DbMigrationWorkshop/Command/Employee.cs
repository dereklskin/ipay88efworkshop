﻿using Domain.DbMigrationWorkshop;
using System;

namespace Application.DbMigrationWorkshop.Command
{
    public class Employee
    {
        public long EmployeeId { get; set; }
        public string Name { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public string JobTitle { get; set; }
        public string PhoneNumber { get; set; }
        public string Remarks { get; set; }
        public DateTime HiredDate { get; set; }

        public Employee(long employeeId, string name, MaritalStatus maritalStatus, string jobTitle, string phoneNumber, string remarks, DateTime hiredDate)
        {
            this.EmployeeId = employeeId;
            this.Name = name;
            this.MaritalStatus = maritalStatus;
            this.JobTitle = jobTitle;
            this.PhoneNumber = phoneNumber;
            this.Remarks = remarks;
            this.HiredDate = hiredDate;
        }
    }
}
