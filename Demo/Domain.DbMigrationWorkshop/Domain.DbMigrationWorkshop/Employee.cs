﻿using System;

namespace Domain.DbMigrationWorkshop
{
    public class Employee
    {
        public long EmployeeId { get; private set; }
        public string Name { get; private set; }
        public MaritalStatus MaritalStatus { get; private set; }
        public string JobTitle { get; private set; }
        public string PhoneNumber { get; private set; }
        public string Remarks { get; private set; }
        public DateTime HiredDate { get; private set; }

        public Employee(long employeeId, string name, MaritalStatus maritalStatus, string jobTitle, string phoneNumber, string remarks, DateTime hiredDate)
        {
            this.EmployeeId = employeeId;
            this.Name = name;
            this.MaritalStatus = maritalStatus;
            this.JobTitle = jobTitle;
            this.PhoneNumber = phoneNumber;
            this.Remarks = remarks;
            this.HiredDate = hiredDate;
        }
    }
}
