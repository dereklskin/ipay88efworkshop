﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DbMigrationWorkshop
{
    public class Salary
    {
        public long SalaryId { get; private set; }
        public long EmployeeId { get; private set; }
        public decimal BasicSalary { get; private set; }
        public decimal? Bonus { get; private set; }
        public decimal? IncrementPercentage { get; private set; }
        public DateTime ReleaseDate { get; private set; }
        public string test { get; set; }

        public Employee Employee { get; private set; }

        public Salary(long salaryId, long employeeId, decimal basicSalary, decimal? bonus, decimal? incrementPercentage, DateTime releaseDate)
        {
            this.SalaryId = salaryId;
            this.EmployeeId = employeeId;
            this.BasicSalary = basicSalary;
            this.Bonus = bonus;
            this.IncrementPercentage = incrementPercentage;
            this.ReleaseDate = releaseDate;
        }
    }
}
