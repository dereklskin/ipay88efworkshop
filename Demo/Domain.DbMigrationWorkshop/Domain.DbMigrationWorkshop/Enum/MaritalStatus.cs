﻿namespace Domain.DbMigrationWorkshop
{
    public enum MaritalStatus
    {
        Undefined = 0,
        Single = 1,
        Married = 2
    }
}
