using App = Application.DbMigrationWorkshop.Command;
using Domain.DbMigrationWorkshop;
using Infrastructure.DbMigrationWorkshop.Database;
using Infrastructure.DbMigrationWorkshop.Domain;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace Api.Hosting
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private const string connectionStringKey = "Demo";
        public Startup(IConfiguration configuration)
        {
            this._configuration = new ConfigurationBuilder()
                   .AddJsonFile("appsettings.json")
                   .Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = this._configuration.GetConnectionString(connectionStringKey);

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Api.Hosting", Version = "v1" });
            });

            services.AddMediatR(typeof(Startup).Assembly);
            services.AddTransient<IRequestHandler<App.GetEmployeeCommand, List<App.Employee>>, App.GetEmployeeCommandHandler>();
            services.AddTransient<IRequestHandler<App.AddEmployeeCommand, bool>, App.AddEmployeeCommandHandler>();

            services.AddTransient<IDemoRepository, DemoRepository>();
            services.AddDbContext<DemoDbContext>(options => options.UseSqlServer(connectionString,
                    sql =>
                    {
                        sql.MigrationsAssembly("Infrastructure.DbMigrationWorkshop");
                        sql.MigrationsHistoryTable("history", "Demo_migration");
                    }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Demo Hosting v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
