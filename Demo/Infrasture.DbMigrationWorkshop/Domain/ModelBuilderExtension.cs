﻿using Domain.DbMigrationWorkshop;
using Microsoft.EntityFrameworkCore;
using System;

namespace Infrastructure.DbMigrationWorkshop.Domain
{
    public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().HasData(
                    new Employee(1, "Yong", MaritalStatus.Married, "Product Owner", "011-11119876", "seeding data", DateTime.Parse("2022-05-01")),
                    new Employee(2, "Alex Pee", MaritalStatus.Married, "Solution Architect", "012-22229876", "seeding data", DateTime.Parse("2022-05-01")),
                    new Employee(3, "Angelika", MaritalStatus.Single, "Senior Software Engineer", "013-33339876", "seeding data", DateTime.Parse("2022-05-01")),
                    new Employee(4, "Derek Liew", MaritalStatus.Single, "Cleaner", "014-44449876", "seeding data", DateTime.Parse("2022-06-01"))
                );

            modelBuilder.Entity<Salary>().HasData(
                    new Salary(1, 1, 5000, 1000, 10, DateTime.Parse("2022-05-28")),
                    new Salary(2, 1, 5000, null, 8, DateTime.Parse("2022-06-28")),
                    new Salary(3, 2, 4000, 500, 6, DateTime.Parse("2022-05-28")),
                    new Salary(4, 3, 3000, 400, 6, DateTime.Parse("2022-05-28")),
                    new Salary(6, 4, 1000, 50, 6, DateTime.Parse("2022-06-28"))
                );
        }
    }
}
