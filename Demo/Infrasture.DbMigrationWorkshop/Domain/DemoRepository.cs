﻿using Domain.DbMigrationWorkshop;
using Infrastructure.DbMigrationWorkshop.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.DbMigrationWorkshop.Domain
{
    public class DemoRepository : IDemoRepository
    {
        private readonly DemoDbContext _dbContext;

        public DemoRepository(DemoDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async void AddEmployeeAsync(Employee employee)
        {
            await _dbContext.AddAsync(employee);
            _dbContext.SaveChanges();
        }

        public async Task<List<Employee>> GetAllEmployeeAsync()
        {
            return await Task.Run(()=> _dbContext.Employee.ToList());
        }
    }
}
