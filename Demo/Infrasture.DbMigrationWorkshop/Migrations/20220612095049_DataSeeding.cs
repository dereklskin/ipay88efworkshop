﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.DbMigrationWorkshop.Migrations
{
    public partial class DataSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "Demo",
                table: "Employee",
                columns: new[] { "EmployeeId", "HiredDate", "JobTitle", "MaritalStatus", "Name", "PhoneNumber", "Remarks" },
                values: new object[,]
                {
                    { 1L, new DateTime(2022, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Product Owner", (short)2, "Yong", "011-11119876", "seeding data" },
                    { 2L, new DateTime(2022, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Solution Architect", (short)2, "Alex Pee", "012-22229876", "seeding data" },
                    { 3L, new DateTime(2022, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Senior Software Engineer", (short)1, "Angelika", "013-33339876", "seeding data" },
                    { 4L, new DateTime(2022, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Cleaner", (short)1, "Derek Liew", "014-44449876", "seeding data" }
                });

            migrationBuilder.InsertData(
                schema: "Demo",
                table: "Salary",
                columns: new[] { "SalaryId", "BasicSalary", "Bonus", "EmployeeId", "IncrementPercentage", "ReleaseDate", "test" },
                values: new object[,]
                {
                    { 1L, 5000m, 1000m, 1L, 10m, new DateTime(2022, 5, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 2L, 5000m, null, 1L, 8m, new DateTime(2022, 6, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 3L, 4000m, 500m, 2L, 6m, new DateTime(2022, 5, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 4L, 3000m, 400m, 3L, 6m, new DateTime(2022, 5, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 6L, 1000m, 50m, 4L, 6m, new DateTime(2022, 6, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "Demo",
                table: "Employee",
                keyColumn: "EmployeeId",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                schema: "Demo",
                table: "Employee",
                keyColumn: "EmployeeId",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                schema: "Demo",
                table: "Employee",
                keyColumn: "EmployeeId",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                schema: "Demo",
                table: "Employee",
                keyColumn: "EmployeeId",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                schema: "Demo",
                table: "Salary",
                keyColumn: "SalaryId",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                schema: "Demo",
                table: "Salary",
                keyColumn: "SalaryId",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                schema: "Demo",
                table: "Salary",
                keyColumn: "SalaryId",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                schema: "Demo",
                table: "Salary",
                keyColumn: "SalaryId",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                schema: "Demo",
                table: "Salary",
                keyColumn: "SalaryId",
                keyValue: 6L);
        }
    }
}
