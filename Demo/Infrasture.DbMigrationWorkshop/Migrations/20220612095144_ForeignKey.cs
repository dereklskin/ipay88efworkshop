﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.DbMigrationWorkshop.Migrations
{
    public partial class ForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Salary_EmployeeId",
                schema: "Demo",
                table: "Salary",
                column: "EmployeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Salary_Employee_EmployeeId",
                schema: "Demo",
                table: "Salary",
                column: "EmployeeId",
                principalSchema: "Demo",
                principalTable: "Employee",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Salary_Employee_EmployeeId",
                schema: "Demo",
                table: "Salary");

            migrationBuilder.DropIndex(
                name: "IX_Salary_EmployeeId",
                schema: "Demo",
                table: "Salary");
        }
    }
}
