﻿using Domain.DbMigrationWorkshop;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.DbMigrationWorkshop.TypeConfigurationManager
{
    internal class EmployeeEntityTypeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable(typeof(Employee).Name, SchemaName.Demo);
            builder.Property(p => p.EmployeeId).ValueGeneratedNever();
            builder.HasKey(b => b.EmployeeId);

            builder.Property(p => p.Name).HasMaxLength(100).IsRequired();
            builder.Property(p => p.MaritalStatus).HasColumnType("smallint");
            builder.Property(p => p.JobTitle).HasColumnType("varchar").HasMaxLength(50).IsRequired();
            builder.Property(p => p.PhoneNumber).HasColumnType("varchar").HasMaxLength(15);
            builder.Property(p => p.Remarks).HasMaxLength(250);
            builder.Property(p => p.HiredDate).IsRequired();
        }
    }
}
