﻿using Domain.DbMigrationWorkshop;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.DbMigrationWorkshop.TypeConfigurationManager
{
    internal class SalaryEntityTypeConfiguration : IEntityTypeConfiguration<Salary>
    {
        public void Configure(EntityTypeBuilder<Salary> builder)
        {
            builder.ToTable(typeof(Salary).Name, SchemaName.Demo);
            builder.Property(p => p.SalaryId).ValueGeneratedNever();
            builder.HasKey(b => b.SalaryId);

            builder.Property(p => p.BasicSalary).HasColumnType("money").HasDefaultValue(0);
            builder.Property(p => p.Bonus).HasColumnType("money");
            builder.Property(p => p.IncrementPercentage).HasColumnType("decimal(5,2)");

            builder.HasOne(x => x.Employee)
                .WithMany()
                .HasForeignKey(z => z.EmployeeId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
