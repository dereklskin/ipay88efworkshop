﻿using Application.DbMigrationWorkshop.Command;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using Dom = Domain.DbMigrationWorkshop;

namespace Api.Hosting.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : Controller
    {
        private readonly IMediator _mediator;

        public EmployeeController(Dom.IExerciseRepository exerciseRepo, IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllEmployee()
        {
            var handler = new GetEmployeeCommand();
            var result = await _mediator.Send(handler);

            if (result.Count > 0)
            {
                return Ok(result);
            }
            else {
                return NotFound(result);
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddEmployee([FromBody] Employee employee)
        {
            var handler = new AddEmployeeCommand(employee);
            var result = await _mediator.Send(handler);
            return Ok(result);
        }
    }
}
