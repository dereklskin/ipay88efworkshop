﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dom = Domain.DbMigrationWorkshop;

namespace Application.DbMigrationWorkshop.Command
{
    public class GetEmployeeCommandHandler : IRequestHandler<GetEmployeeCommand, List<Employee>>
    {
        private readonly Dom.IExerciseRepository _exerciseRepo;
        public GetEmployeeCommandHandler(Dom.IExerciseRepository exerciseRepo)
        {
            this._exerciseRepo = exerciseRepo;
        }

        public async Task<List<Employee>> Handle(GetEmployeeCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
