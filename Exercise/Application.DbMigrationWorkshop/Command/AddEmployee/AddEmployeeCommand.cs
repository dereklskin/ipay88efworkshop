﻿using MediatR;

namespace Application.DbMigrationWorkshop.Command
{
    public class AddEmployeeCommand : IRequest<bool>
    {
        public Employee Employee { set; get; }
        public AddEmployeeCommand(Employee employee)
        {
            this.Employee = employee;
        }
    }
}
