﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Dom = Domain.DbMigrationWorkshop;

namespace Application.DbMigrationWorkshop.Command
{
    public class AddEmployeeCommandHandler : IRequestHandler<AddEmployeeCommand, bool>
    {
        private readonly Dom.IExerciseRepository _exerciseRepo;
        public AddEmployeeCommandHandler(Dom.IExerciseRepository exerciseRepo)
        {
            this._exerciseRepo = exerciseRepo;
        }

        public async Task<bool> Handle(AddEmployeeCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
