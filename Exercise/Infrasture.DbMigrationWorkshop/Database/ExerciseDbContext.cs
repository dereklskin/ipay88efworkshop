﻿using Domain.DbMigrationWorkshop;
using Infrastructure.DbMigrationWorkshop.Domain;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Infrastructure.DbMigrationWorkshop.Database
{
    public class ExerciseDbContext : DbContext
    {
        public DbSet<Employee> Employee { get; set; }

        public ExerciseDbContext(DbContextOptions<ExerciseDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            modelBuilder.Seed();
        }
    }
}
