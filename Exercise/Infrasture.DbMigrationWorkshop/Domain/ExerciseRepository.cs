﻿using Domain.DbMigrationWorkshop;
using Infrastructure.DbMigrationWorkshop.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.DbMigrationWorkshop.Domain
{
    public class ExerciseRepository : IExerciseRepository
    {
        private readonly ExerciseDbContext _dbContext;

        public ExerciseRepository(ExerciseDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async void AddEmployeeAsync(Employee employee)
        {
            await _dbContext.AddAsync(employee);
            _dbContext.SaveChanges();
        }

        public async Task<List<Employee>> GetAllEmployeeAsync()
        {
            return await Task.Run(()=> _dbContext.Employee.ToList());
        }
    }
}
