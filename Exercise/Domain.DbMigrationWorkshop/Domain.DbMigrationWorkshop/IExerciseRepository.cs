﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DbMigrationWorkshop
{
    public interface IExerciseRepository
    {
        Task<List<Employee>> GetAllEmployeeAsync();
        void AddEmployeeAsync(Employee employee);
    }
}
